from bonus_system import calculateBonuses

AMOUNTS = [0, 10000, 10001, 50000, 50001, 100000, 100001, 500000, 500001]
RESULTS = [1, 1.5, 1.5, 2, 2, 2.5, 2.5]


def test_standard():
    program = 'Standard'
    for amount, result in zip(AMOUNTS, RESULTS):
        assert calculateBonuses(program, amount) == 0.5 * result


def test_premium():
    program = 'Premium'
    for amount, result in zip(AMOUNTS, RESULTS):
        assert calculateBonuses(program, amount) == 0.1 * result


def test_diamond():
    program = 'Diamond'
    for amount, result in zip(AMOUNTS, RESULTS):
        assert calculateBonuses(program, amount) == 0.2 * result


def test_nonsense():
    programs = ['Nonsense', 'Standar', 'Standarda', 'Premiu', 'Premiuma', 'Diamon', 'Diamonda']
    for amount in AMOUNTS:
        for program in programs:
            assert calculateBonuses(program, amount) == 0
